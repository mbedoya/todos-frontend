var gulp = require('gulp');
var pump = require('pump');

gulp.task('default', function (callback) {
    pump([
        gulp.src('dist/*.js'),
        gulp.dest('build/dist'),
        gulp.src('index.html'),
        gulp.dest('build')
    ], callback);
});