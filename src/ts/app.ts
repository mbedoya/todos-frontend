import 'bootstrap/dist/css/bootstrap.min.css';
import './../css/clean-blog.min.css';

import { AppController } from "./app.controller";

(function () {
    new AppController();
})();