import * as $ from "jquery";
import { Todo } from "./app.interface"

export class AppService {
    constructor() {

    }

    getAllTodos(success, error): void{

        const apiUri = "https://test-todos-api.azurewebsites.net/api/todos";
        //url: "http://localhost:51886/api/todos; https://jsonplaceholder.typicode.com/posts"

        $.ajax({
            method: "GET",
            url: apiUri
        })
        .done(success)
        .fail(error);
    }
}