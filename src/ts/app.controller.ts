import { Todo } from './app.interface';
import { AppService } from "./app.service";

export class AppController {

    mainContainer: any;
    todosContainer: any;
    todoInput: any;
    appService: AppService;
    todosToDisplay: Todo[];
    welcomeMessage = "Let's have Fun!";
    maxTodosToDisplay = 10;

    constructor() {

        this.appService = new AppService();
        window.addEventListener("load", this.onInit.bind(this));
    }

    onInit(): void {
        this.mainContainer = document.getElementById("main-container");
        this.todosContainer = document.getElementById("todos-container");
        this.todoInput = document.getElementById("todoInput");

        document.getElementById("addTodoButton").addEventListener("click", this.onAddTodo.bind(this));

        this.mainContainer.innerHTML = this.welcomeMessage;

        this.appService.getAllTodos(
            (data) => {
                this.todosToDisplay = data;
                this.updateView();
            },
            (error) => {
                console.log(error);
            }
        );
    }

    onAddTodo(): void {
        alert(this.todoInput.value);
    }

    updateView(): void {

        let htmlPostContent = '';
        let todosDisplayed = 0;

        while (todosDisplayed < this.maxTodosToDisplay && todosDisplayed < this.todosToDisplay.length) {

            const content = this.todosToDisplay[todosDisplayed];

            htmlPostContent +=
                '<div class="post-preview">' +
                '<a href="#">' +
                '<h2 class="post-title">' +
                content.title +
                '</h2>' +
                '<h3 class="post-subtitle">' +
                content.body +
                '</h3>' +
                '</a>' +
                '<p class="post-meta">Posted by ' +
                '<a href="#">' + "" + '</a> on September 24, 2014</p>' +
                '</div>' +
                '<hr>';

            todosDisplayed++;
        }
        
        this.todosContainer.innerHTML = htmlPostContent;
    }

}